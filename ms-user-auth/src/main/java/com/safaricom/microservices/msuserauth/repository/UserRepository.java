package com.safaricom.microservices.msuserauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.safaricom.microservices.msuserauth.model.User;

public interface UserRepository extends JpaRepository<User, Integer>{
	User findByUsername(String username);
	User findByUsernameAndPassword(String username,String password);
}
