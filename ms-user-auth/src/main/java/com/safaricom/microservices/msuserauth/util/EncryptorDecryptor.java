package com.safaricom.microservices.msuserauth.util;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;


public class EncryptorDecryptor {
	 public EncryptorDecryptor() {
		    // Create a constructor
		  }

		  public static  String encrypt(String rawText, String rawSecretKey) throws NoSuchAlgorithmException, NoSuchPaddingException,
		      InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		    SecretKey aesKey = new SecretKeySpec(rawSecretKey.getBytes(), "AES");
		    Cipher cipher = Cipher.getInstance("AES");
		    byte[] encryptedTextBytes = rawText.getBytes();
		    cipher.init(Cipher.ENCRYPT_MODE, aesKey);
		    byte[] encryptedByte = cipher.doFinal(encryptedTextBytes);
		    Base64.Encoder encoder = Base64.getEncoder();
		    String encryptedText = encoder.encodeToString(encryptedByte);
		    return encryptedText;
		  }

		  public static String decrypt(String encryptedText, String secretKey) throws NoSuchAlgorithmException,
		      NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		    SecretKey aesKey = new SecretKeySpec(secretKey.getBytes(), "AES");
		    Cipher cipher = Cipher.getInstance("AES");
		    Base64.Decoder decoder = Base64.getDecoder();
		    byte[] encryptedTextByte = decoder.decode(encryptedText);
		    cipher.init(Cipher.DECRYPT_MODE, aesKey);
		    byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
		    return new String(decryptedByte);

		  }

		 

}
