package com.safaricom.microservices.msuserauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsUserAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsUserAuthApplication.class, args);
	}

}
