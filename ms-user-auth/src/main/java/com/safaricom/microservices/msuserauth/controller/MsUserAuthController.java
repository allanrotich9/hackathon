package com.safaricom.microservices.msuserauth.controller;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.safaricom.microservices.msuserauth.model.JsonResponse;
import com.safaricom.microservices.msuserauth.model.User;
import com.safaricom.microservices.msuserauth.model.UserRequest;
import com.safaricom.microservices.msuserauth.repository.UserRepository;
import com.safaricom.microservices.msuserauth.util.Bcrypt;
import com.safaricom.microservices.msuserauth.util.ConfigProperties;
import com.safaricom.microservices.msuserauth.util.EncryptorDecryptor;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.swagger.annotations.ApiOperation;

@RestController
public class MsUserAuthController {

	private Logger logger = LoggerFactory.getLogger(MsUserAuthController.class);

	@Autowired
	ConfigProperties config;

	@Autowired
	UserRepository userRepo;

	Bcrypt bycrpt = new Bcrypt(12);

	private static final String secret = "JwtSecretKey";
	private static final byte[] secretBytes = secret.getBytes();
	private static final String baseSecretBytes = Base64.getEncoder().encodeToString(secretBytes);
	private static final String status = "status";

	@ApiOperation(value = "This endpoint for login", response = JsonResponse.class)
	@PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> login(@RequestBody UserRequest body, @RequestHeader HttpHeaders httpHeaders) {
		final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		final UUID uuid = UUID.randomUUID();
		final String referenceId = uuid.toString();
		JsonResponse response = new JsonResponse();
		if (body.getUsername() == null || body.getPassword() == null) {
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=User | SourceSystem=UserService | ResponseCode=403 | ResponseMsg=Username and Password are required | Headers={} |",
					referenceId, timestamp, httpHeaders);
			response.setRes_code("400");
			response.setRes_msg("Username and Password are required");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		if (userRepo.findByUsername(body.getUsername()) == null) {
			response.setRes_code("403");
			response.setRes_msg("User not registered");
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=User | SourceSystem=UserService | ResponseCode=403 | ResponseMsg=User not registered | Headers={} |",
					referenceId, timestamp, httpHeaders);
			return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
		}
		User user=userRepo.findByUsername(body.getUsername());
		
		try {
			if(decryptedText(user.getPassword()).equals(body.getPassword())) {
				HashMap<String, Object> map = new HashMap<>();
				String token = generateToken(user.getId(), body.getUsername(),config.getJwtDuration());
				map.put("res_code", "200");
				map.put("res_msg", "Success");
				map.put("token", token);
				logger.trace(
						"TransactionID={} | Timestamp={} | TransactionType=User | SourceSystem=UserService | ResponseCode=200 | ResponseMsg=Success | Headers={} |",
						referenceId, timestamp, httpHeaders);
				return new ResponseEntity<>(map, HttpStatus.OK);
			}else {
				response.setRes_code("403");
				response.setRes_msg("Invalid Password");
				logger.error(
						"TransactionID={} | Timestamp={} | TransactionType=User | SourceSystem=UserService | ResponseCode=403 | ResponseMsg=Invalid Password | Headers={} |",
						referenceId, timestamp, httpHeaders);
				return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
			}
		} catch (Exception ex) {
			response.setRes_code("500");
			response.setRes_msg("Internal Server Error");
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=User | SourceSystem=UserService | ResponseCode=500 | ResponseMsg=Internal Server Error | Headers={} |",
					referenceId, timestamp, httpHeaders);
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	

	}

	@ApiOperation(value = "This endpoint for registering user", response = JsonResponse.class)
	@PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registration(@RequestBody UserRequest body, @RequestHeader HttpHeaders httpHeaders) {
		final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		final UUID uuid = UUID.randomUUID();
		final String referenceId = uuid.toString();
		JsonResponse response = new JsonResponse();
		if (body.getUsername() == null || body.getPassword() == null) {
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=User | SourceSystem=UserService | ResponseCode=403 | ResponseMsg=Username and Password are required | Headers={} |",
					referenceId, timestamp, httpHeaders);
			response.setRes_code("400");
			response.setRes_msg("Username and Password are required");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		if (userRepo.findByUsername(body.getUsername()) != null) {
			response.setRes_code("409");
			response.setRes_msg("Username already registered");
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=User | SourceSystem=UserService | ResponseCode=409 | ResponseMsg=Conflict | Headers={} |",
					referenceId, timestamp, httpHeaders);
			return new ResponseEntity<>(response, HttpStatus.CONFLICT);
		}
		try {
			User user = new User();
			user.setUsername(body.getUsername());
			user.setPassword(encryptText(body.getPassword()));
			userRepo.save(user);
			HashMap<String, Object> map = new HashMap<>();
			String token = generateToken(user.getId(), body.getUsername(),config.getJwtDuration());
			map.put("res_code", "200");
			map.put("res_msg", "Success");
			map.put("token", token);
			logger.trace(
					"TransactionID={} | Timestamp={} | TransactionType=User | SourceSystem=UserService | ResponseCode=200 | ResponseMsg=Success | Headers={} |",
					referenceId, timestamp, httpHeaders);
			return new ResponseEntity<>(map, HttpStatus.OK);
		} catch (Exception ex) {
			response.setRes_code("500");
			response.setRes_msg("Internal Server Error");
			logger.error(
					"TransactionID={} | Timestamp={} | TransactionType=User | SourceSystem=UserService | ResponseCode=500 | ResponseMsg=Internal Server Error | Headers={} |",
					referenceId, timestamp, httpHeaders);
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	private static String generateToken(Integer id, String email,Integer duration) {
		Date exp = new Date(System.currentTimeMillis() + duration); // 5 mins

		String token = Jwts.builder().setSubject(email).setExpiration(exp)
				.signWith(SignatureAlgorithm.HS256, baseSecretBytes).compact();

		return token;
	}

	private String decryptedText(String text) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		return EncryptorDecryptor.decrypt(text, config.getSecretKey());
	}

	private String encryptText(String text) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		return EncryptorDecryptor.encrypt(text, config.getSecretKey());
	}
}
