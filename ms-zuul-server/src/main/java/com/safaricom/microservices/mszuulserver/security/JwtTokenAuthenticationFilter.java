package com.safaricom.microservices.mszuulserver.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.safaricom.microservices.mscommon.security.JwtConfig;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;



public class JwtTokenAuthenticationFilter extends OncePerRequestFilter {
	private static final Logger logger = LoggerFactory.getLogger(JwtTokenAuthenticationFilter.class);
	private final JwtConfig jwtConfig;
	public JwtTokenAuthenticationFilter(JwtConfig jwtConfig) {
		this.jwtConfig = jwtConfig;
	}
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "*");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "*");
		// 1. get the authentication header. Tokens are supposed to be passed in the
		// authentication header
		String header = request.getHeader("Authorization");
		
		// 2. validate the header and check the prefix
		if (header == null) {
			chain.doFilter(request, response); // If not valid, go to the next filter.
			return;
		}
		// If there is no token provided and hence the user won't be authenticated.
		// It's Ok. Maybe the user accessing a public path or asking for a token.
		// All secured paths that needs a token are already defined and secured in
		// config class.
		// And If user tried to access without access token, then he won't be
		// authenticated and an exception will be thrown.
		// 3. Get the token
		String token = header.replace(jwtConfig.getPrefix(), "");	
		try { // exceptions might be thrown in creating the claims if for example the token is
				// expired
			// 4. Validate the token
		
			Claims claims = Jwts.parser().setSigningKey(jwtConfig.getSecret().getBytes())
					.parseClaimsJws(token)
					.getBody();
			String username = claims.getSubject();
			
			if (username != null) {
				@SuppressWarnings("unchecked")
				UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
						username, null,
						null);
				// 6. Authenticate the user
				// Now, user is authenticated
				SecurityContextHolder.getContext().setAuthentication(auth);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			// In case of failure. Make sure it's clear; so guarantee user won't be
			// authenticated
			SecurityContextHolder.clearContext();
		}
		// go to the next filter in the filter chain
		
		chain.doFilter(request, response);
	}
	
}
