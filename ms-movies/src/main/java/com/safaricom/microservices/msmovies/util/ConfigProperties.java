package com.safaricom.microservices.msmovies.util;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * This class is used to manage configuration in our Spring application.
 *
 * It is important to help us refresh config variables such as base-url during
 * runtime to avoid restarting our application.
 *
 * It contains getters and setters of the specific configuration values that are
 * dynamic and might need to change during runtime.
 *
 * @author Allan Rotich
 */
@Component
@ConfigurationProperties
public class ConfigProperties {

    private String secretKey;

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

  
	
    
}