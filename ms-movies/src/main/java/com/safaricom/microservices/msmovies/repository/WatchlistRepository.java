package com.safaricom.microservices.msmovies.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.safaricom.microservices.msmovies.model.Watchlist;

public interface WatchlistRepository  extends JpaRepository<Watchlist, Integer>{

}
